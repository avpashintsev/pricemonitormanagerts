import 'bootstrap/dist/css/bootstrap.css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import rootReducer from "./store/rootReducer";
import {checkAuth} from './store/auth/actions';
import { ConnectedRouter } from 'connected-react-router';
//import { ConnectedRouter } from 'connected-react-router/immutable'; 
import {composeWithDevTools} from "redux-devtools-extension";
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter } from 'react-router-dom';

// Create browser history to use in the Redux store
//const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href') as string;
//const history = createBrowserHistory({ basename: baseUrl });

// wfc Get the application-wide store instance, prepopulating with state from the server where available.
// const store = configureStore(history);

const store = createStore(
    rootReducer,
	composeWithDevTools(applyMiddleware(thunk))
);
export type RootState = ReturnType<typeof rootReducer>
//export const history = createBrowserHistory();
//export const store = configureStore(history);

checkAuth(store);

ReactDOM.render((
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>
), document.getElementById('root'));


registerServiceWorker();
