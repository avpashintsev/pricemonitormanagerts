import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';

import './custom.css'
import { LoginForm } from './components/Login';
import { AddSiteForm } from './components/AddSite';
import { RuleManageForm } from './components/RuleManage';

export default () => (
    <Layout>
        <Route exact path='/' component={LoginForm} />
        <Route path='/addsite' component={AddSiteForm}  />
        <Route path='/rulemanage' component={RuleManageForm} />
    </Layout>
);
