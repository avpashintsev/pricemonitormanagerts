const axiosClient = require('./axiosClient');

export function apiRequest(url) {
	return dispatch => {
		return axiosClient.get(url).then(res => {
				if (res.data.status === 'ok') {
					return res.data.data;
				} else {
					alert(res.data.message);
				}
			},
			err => {
				alert(err.message);
			}
		);
	}
}