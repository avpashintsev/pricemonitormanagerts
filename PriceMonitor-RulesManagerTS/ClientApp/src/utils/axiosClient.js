const axios = require('axios');

const axiosInstance = axios.create({
	 baseURL: 'http://localhost:60286/',
});

module.exports = axiosInstance;