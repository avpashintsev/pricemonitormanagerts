import * as React from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import './NavMenu.css';
import { logOut } from '../store/auth/actions';

export default class NavMenu extends React.PureComponent<{}, { isOpen: boolean }> {
    public state = {
        isOpen: false
    };

    public render() {
        return (            
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3" light>
                    <Container>
                        <NavbarBrand tag={Link} to="/">PriceMonitor - Rules Manager</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} className="mr-2"/>
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={this.state.isOpen} navbar>
                            <ul className="navbar-nav flex-grow">
                                {!localStorage.jwtToken && <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/">LogIn</NavLink>
                                </NavItem>}
                                {localStorage.jwtToken && <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/addsite">Add Site</NavLink>
                                </NavItem>}
                                {localStorage.jwtToken && <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/rulemanage">Rules Manage</NavLink>
                                </NavItem>}
                                {localStorage.jwtToken && <button onClick={logOut()}>logout</button>}
                            </ul>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }

    private toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
}
