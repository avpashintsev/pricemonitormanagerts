"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RuleManageForm = void 0;
var react_hook_form_1 = require("react-hook-form");
var core_1 = require("@emotion/core");
var RuleTableRow_1 = require("./RuleTableRow");
var react_1 = require("react");
var react_router_1 = require("react-router");
var baseCSS = core_1.css(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"], ["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"])));
var fieldsetCSS = core_1.css(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n                    margin: 10px auto 0 auto;\n                    padding: 30px;\n                    width: 650px;\n                    background-color: gray;\n                    border-radius: 4px;\n                    border: 1px solid gray;\n                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);\n                "], ["\n                    margin: 10px auto 0 auto;\n                    padding: 30px;\n                    width: 650px;\n                    background-color: gray;\n                    border-radius: 4px;\n                    border: 1px solid gray;\n                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);\n                "])));
var buttonSearchCss = core_1.css(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n                            border-top: 1px solid gray;\n                        "], ["\n                            border-top: 1px solid gray;\n                        "])));
var initState = { sitename: '', rules: { price: '', rule1: '', rule2: '' } };
exports.RuleManageForm = function () {
    var _a = react_hook_form_1.useForm(), register = _a.register, handleSubmit = _a.handleSubmit, errors = _a.errors;
    var _b = react_1.useState(initState), rulesManage = _b[0], setRulesManage = _b[1];
    var loadData = function (url) {
        var xhr = new XMLHttpRequest();
        xhr.open("get", '/api/rules/findsite?site=' + url, true);
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            if (data[0]) {
                var r = { sitename: data[0].siteUrl, rules: { price: data[0].priceRule, rule1: data[0].rule1, rule2: data[0].rule2 } };
                setRulesManage(r);
            }
            else {
                alert("Site not found!");
            }
        };
        xhr.send();
    };
    var onSubmit = function (data) {
        loadData(data.sitename);
    };
    var handleInput = function (name, value) {
        if (rulesManage) {
            var data = {
                sitename: rulesManage.sitename,
                rules: {
                    price: rulesManage === null || rulesManage === void 0 ? void 0 : rulesManage.rules.price,
                    rule1: rulesManage === null || rulesManage === void 0 ? void 0 : rulesManage.rules.rule1,
                    rule2: rulesManage === null || rulesManage === void 0 ? void 0 : rulesManage.rules.rule2,
                }
            };
            switch (name) {
                case 'Price':
                    data.rules.price = value;
                    break;
                case 'Rule1':
                    data.rules.rule1 = value;
                    break;
                case 'Rule2':
                    data.rules.rule2 = value;
                    break;
                default:
            }
            setRulesManage(data);
        }
    };
    var updateData = function (r) {
        //TODO вынести в apiRequests
        var xhr = new XMLHttpRequest();
        xhr.open('PUT', '/api/rules/updatesite');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            if (data[0]) {
                var rr = {
                    sitename: data[0].siteUrl,
                    rules: {
                        price: data[0].priceRule,
                        rule1: data[0].rule1,
                        rule2: data[0].rule2
                    }
                };
                setRulesManage(rr);
            }
            else {
                alert("Site not found!");
            }
        };
        xhr.send(JSON.stringify({ SiteUrl: r.sitename, PriceRule: r.rules.price, Rule1: r.rules.rule1, Rule2: r.rules.rule2 }));
        //setRulesManage(r);
        alert('Rules for ' + r.sitename + ' has been updated!');
    };
    var deleteSite = function (sitename) {
        //TODO вынести в apiRequests
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', '/api/rules/deletesite');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(sitename));
        setRulesManage(initState);
        alert('Site ' + sitename + ' has been deleted!');
    };
    if (!localStorage.jwtToken)
        return (core_1.jsx(react_router_1.Redirect, { to: "/" }));
    return (core_1.jsx("form", { onSubmit: handleSubmit(onSubmit) },
        core_1.jsx("fieldset", { css: fieldsetCSS },
            core_1.jsx("div", { className: "field" },
                core_1.jsx("label", { htmlFor: "sitename" }, "Find Site by URL"),
                core_1.jsx("input", { css: baseCSS, type: "text", id: "sitename", name: "sitename", ref: register({ required: true }) }),
                errors.sitename && errors.sitename.type === "required" && (core_1.jsx("div", { className: "error" }, "Your must enter sitename."))),
            core_1.jsx("div", { css: buttonSearchCss },
                core_1.jsx("button", { type: "submit", className: "btn btn-primary btn-lg" }, "Find Site")),
            core_1.jsx("div", { className: "rules" },
                core_1.jsx("label", null,
                    "Site URL: ", rulesManage === null || rulesManage === void 0 ? void 0 :
                    rulesManage.sitename),
                core_1.jsx("table", null,
                    core_1.jsx(RuleTableRow_1.RuleTableRow, { name: 'Price', value: rulesManage === null || rulesManage === void 0 ? void 0 : rulesManage.rules.price, handle: handleInput }),
                    core_1.jsx(RuleTableRow_1.RuleTableRow, { name: 'Rule1', value: rulesManage === null || rulesManage === void 0 ? void 0 : rulesManage.rules.rule1, handle: handleInput }),
                    core_1.jsx(RuleTableRow_1.RuleTableRow, { name: 'Rule2', value: rulesManage === null || rulesManage === void 0 ? void 0 : rulesManage.rules.rule2, handle: handleInput }))),
            core_1.jsx("button", { type: "button", className: "btn btn-primary btn-lg", onClick: function () { return updateData(rulesManage); } }, "Update"),
            core_1.jsx("button", { type: "button", className: "btn btn-lg", css: core_1.css(templateObject_4 || (templateObject_4 = __makeTemplateObject(["background-color:red;margin-left: 20px;"], ["background-color:red;margin-left: 20px;"]))), onClick: function () { return deleteSite(rulesManage.sitename); } }, "Delete"))));
};
var templateObject_1, templateObject_2, templateObject_3, templateObject_4;
//# sourceMappingURL=RuleManage.js.map