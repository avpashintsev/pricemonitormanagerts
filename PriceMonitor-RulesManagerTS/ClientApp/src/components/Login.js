"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginForm = void 0;
var react_hook_form_1 = require("react-hook-form");
var core_1 = require("@emotion/core");
var actions_1 = require("../store/auth/actions");
var react_redux_1 = require("react-redux");
var baseCSS = core_1.css(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"], ["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"])));
var fieldsetCSS = core_1.css(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n                    margin: 10px auto 0 auto;\n                    padding: 30px;\n                    width: 350px;\n                    background-color: gray;\n                    border-radius: 4px;\n                    border: 1px solid gray;\n                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);\n                "], ["\n                    margin: 10px auto 0 auto;\n                    padding: 30px;\n                    width: 350px;\n                    background-color: gray;\n                    border-radius: 4px;\n                    border: 1px solid gray;\n                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);\n                "])));
var buttonCss = core_1.css(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n                            margin: 30px 0px 0px 0px;\n                            padding: 20px 0px 0px 0px;   \n                            border-top: 1px solid gray;\n                        "], ["\n                            margin: 30px 0px 0px 0px;\n                            padding: 20px 0px 0px 0px;   \n                            border-top: 1px solid gray;\n                        "])));
exports.LoginForm = function () {
    var dispatch = react_redux_1.useDispatch();
    var _a = react_hook_form_1.useForm(), register = _a.register, handleSubmit = _a.handleSubmit, errors = _a.errors;
    var onSubmit = function (data) {
        //alert(data.name);
        dispatch(actions_1.logIn(data)).then(function () {
            //setLoading(false);
        });
    };
    return (core_1.jsx("form", { onSubmit: handleSubmit(onSubmit) },
        core_1.jsx("fieldset", { css: fieldsetCSS },
            core_1.jsx("div", { className: "field" },
                core_1.jsx("label", { htmlFor: "name" }, "Name"),
                core_1.jsx("input", { css: baseCSS, type: "text", id: "name", name: "name", ref: register({ required: true }) }),
                errors.name && errors.name.type === "required" && (core_1.jsx("div", { className: "error" }, "Your must enter your name."))),
            core_1.jsx("div", { className: "field" },
                core_1.jsx("label", { htmlFor: "password" }, "Name"),
                core_1.jsx("input", { css: baseCSS, type: "password", id: "password", name: "password", ref: register({ required: true }) }),
                errors.name && errors.name.type === "required" && (core_1.jsx("div", { className: "error" }, "Your must enter your password."))),
            core_1.jsx("div", { css: buttonCss },
                core_1.jsx("button", { type: "submit", className: "btn btn-primary btn-lg" }, "Log in")))));
};
var templateObject_1, templateObject_2, templateObject_3;
//# sourceMappingURL=Login.js.map