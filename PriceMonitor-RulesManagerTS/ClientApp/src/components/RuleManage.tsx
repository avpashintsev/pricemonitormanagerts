﻿/** @jsx jsx */
import * as React from 'react';
import { useForm } from "react-hook-form";
import { css, jsx } from '@emotion/core'
import { RuleTableRow } from './RuleTableRow';
import { useState } from 'react';
import { Redirect } from 'react-router';
//let axiosClient = require('../utils/axiosClient');

type Rules = {
    price: string;
    rule1: string;
    rule2: string;
};

type RuleManage = {
    sitename: string;
    rules: Rules
};


const baseCSS = css`
    box-sizing:border-box;
    margin-bottom:5px;
    padding:8px 10px;
    border: 1px solid gray;
    border-radius:3px;
    color:gray;
    background-color:white;
    width:100%;
    :focus{
        outline-color: #898888;
    }
    :disabled{
        outline-color: #adaaaa;
    }
`
const fieldsetCSS = css`
                    margin: 10px auto 0 auto;
                    padding: 30px;
                    width: 650px;
                    background-color: gray;
                    border-radius: 4px;
                    border: 1px solid gray;
                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);
                `

const buttonSearchCss = css`
                            border-top: 1px solid gray;
                        `


var initState: RuleManage = { sitename: '', rules: { price: '', rule1: '', rule2: '' } };


export const RuleManageForm = () => {
    const { register, handleSubmit, errors } = useForm<RuleManage>();
    const [rulesManage, setRulesManage] = useState<RuleManage>(initState);

    const loadData = (url: string) => {
        var xhr = new XMLHttpRequest();
        xhr.open("get", '/api/rules/findsite?site=' + url, true);
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            if (data[0]) {
                let r: RuleManage = { sitename: data[0].siteUrl, rules: { price: data[0].priceRule, rule1: data[0].rule1, rule2: data[0].rule2} };
                setRulesManage(r);
            } else {
                alert("Site not found!")
            }
        };
        xhr.send();
    }

    const onSubmit = (data: RuleManage) => {
        loadData(data.sitename);
    };

    const handleInput = (name:string, value: string) => {
        if (rulesManage) {
            var data: RuleManage = {
                sitename: rulesManage.sitename,
                rules: {
                    price: rulesManage?.rules.price,
                    rule1: rulesManage?.rules.rule1,
                    rule2: rulesManage?.rules.rule2,
                }
            };
            switch (name) {
                case 'Price': data.rules.price = value;
                    break;
                case 'Rule1': data.rules.rule1 = value;
                    break;
                case 'Rule2': data.rules.rule2 = value;
                    break;
                default:
            }
            setRulesManage(data);
        }
    }

    const updateData = (r: RuleManage): void => {
        //TODO вынести в apiRequests
        var xhr = new XMLHttpRequest();        
        xhr.open('PUT', '/api/rules/updatesite');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            if (data[0]) {
                let rr: RuleManage = {
                    sitename: data[0].siteUrl,
                    rules: {
                        price: data[0].priceRule,
                        rule1: data[0].rule1,
                        rule2: data[0].rule2
                    }
                };
                setRulesManage(rr);
            } else {
                alert("Site not found!")
            }
        };
        xhr.send(JSON.stringify({ SiteUrl: r.sitename, PriceRule: r.rules.price, Rule1: r.rules.rule1, Rule2: r.rules.rule2 }));
        //setRulesManage(r);
        alert('Rules for ' + r.sitename + ' has been updated!');
    }

    const deleteSite = (sitename: string): void => {
        //TODO вынести в apiRequests
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', '/api/rules/deletesite');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.setRequestHeader("Content-Type", "application/json");           
        xhr.send(JSON.stringify(sitename));
        setRulesManage(initState); 
        alert('Site ' + sitename + ' has been deleted!');
    }



    if (!localStorage.jwtToken) return (<Redirect to="/" />)
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset css={fieldsetCSS}>
                <div className="field">
                    <label htmlFor="sitename">Find Site by URL</label>
                    <input
                        css={baseCSS}
                        type="text"
                        id="sitename"
                        name="sitename"
                        ref={register({ required: true })}
                    />
                    {errors.sitename && errors.sitename.type === "required" && (<div className="error">Your must enter sitename.</div>)}
                </div>
                <div css={buttonSearchCss}>
                    <button type="submit" className="btn btn-primary btn-lg">Find Site</button>
                </div>
                <div className="rules">
                    <label >Site URL: {rulesManage?.sitename}</label>
                    <table>
                        <RuleTableRow name='Price' value={rulesManage?.rules.price} handle={handleInput} />
                        <RuleTableRow name='Rule1' value={rulesManage?.rules.rule1} handle={handleInput} />
                        <RuleTableRow name='Rule2' value={rulesManage?.rules.rule2} handle={handleInput} />
                    </table>                    
                </div>
                <button type="button" className="btn btn-primary btn-lg" onClick={() => updateData(rulesManage)}>Update</button>
                <button type="button" className="btn btn-lg"
                    css={css`background-color:red;margin-left: 20px;`}
                    onClick={() => deleteSite(rulesManage.sitename)}>Delete</button>


            </fieldset>
        </form>
    );
};