﻿/** @jsx jsx */
import * as React from 'react';
import { useForm } from "react-hook-form";
import { css, jsx } from '@emotion/core'
import { logIn } from '../store/auth/actions';
import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';

type UserLogin = {
    name: string;
    //email: string;
    password: string;
};


const baseCSS = css`
    box-sizing:border-box;
    margin-bottom:5px;
    padding:8px 10px;
    border: 1px solid gray;
    border-radius:3px;
    color:gray;
    background-color:white;
    width:100%;
    :focus{
        outline-color: #898888;
    }
    :disabled{
        outline-color: #adaaaa;
    }
`
const fieldsetCSS = css`
                    margin: 10px auto 0 auto;
                    padding: 30px;
                    width: 350px;
                    background-color: gray;
                    border-radius: 4px;
                    border: 1px solid gray;
                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);
                `

const buttonCss = css`
                            margin: 30px 0px 0px 0px;
                            padding: 20px 0px 0px 0px;   
                            border-top: 1px solid gray;
                        `


export const LoginForm = () => {
    const dispatch: any = useDispatch();
    const { register, handleSubmit, errors } = useForm<UserLogin>();
    const onSubmit = (data: UserLogin) => {
        //alert(data.name);

        dispatch(logIn(data)).then(() => {
            //setLoading(false);
        });
    };
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset css={fieldsetCSS}>
                <div className="field">
                    <label htmlFor="name">Name</label>
                        <input
                                css={baseCSS}
                                type="text"
                                id="name"
                                name="name"
                                ref={register({ required: true })}
                        />
                    {errors.name && errors.name.type === "required" && (<div className="error">Your must enter your name.</div>)}
                </div>
                <div className="field">
                    <label htmlFor="password">Name</label>
                    <input
                        css={baseCSS}
                        type="password"
                        id="password"
                        name="password"
                        ref={register({ required: true })}
                    />
                    {errors.name && errors.name.type === "required" && (<div className="error">Your must enter your password.</div>)}
                </div>
                {/*<div className="field">
                <label htmlFor="email">Email</label>
                    <input
                        css={baseCSS}
                    type="email"
                    id="email"
                    name="email"
                    ref={register({ required: true })}
                />
                {errors.email && errors.email.type === "required" && (<div className="error">Your must enter your email address.</div>)}
                </div>*/}
                    <div css={buttonCss}>
                        <button type="submit" className="btn btn-primary btn-lg">Log in</button>
                    </div>
            </fieldset>
        </form>
    );
};