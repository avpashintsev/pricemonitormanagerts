"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RuleTableRow = void 0;
var core_1 = require("@emotion/core");
var baseCSS = core_1.css(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"], ["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"])));
exports.RuleTableRow = function (_a) {
    var name = _a.name, value = _a.value, handle = _a.handle;
    return core_1.jsx("tr", null,
        core_1.jsx("td", null,
            core_1.jsx("label", { htmlFor: "price" },
                name,
                " rule:")),
        core_1.jsx("td", null,
            core_1.jsx("input", { css: baseCSS, type: "text", id: name, name: name, value: value, onChange: function (ev) { return handle(name, ev.target.value); } })));
};
var templateObject_1;
//# sourceMappingURL=RuleTableRow.js.map