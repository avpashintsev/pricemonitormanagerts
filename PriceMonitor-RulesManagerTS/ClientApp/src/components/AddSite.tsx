﻿/** @jsx jsx */
import * as React from 'react';
import { useForm } from "react-hook-form";
import { css, jsx } from '@emotion/core'
import { Redirect } from 'react-router';
import { useState } from 'react';

type AddSite = {
    sitename: string;
};

const baseCSS = css`
    box-sizing:border-box;
    margin-bottom:5px;
    padding:8px 10px;
    border: 1px solid gray;
    border-radius:3px;
    color:gray;
    background-color:white;
    width:100%;
    :focus{
        outline-color: #898888;
    }
    :disabled{
        outline-color: #adaaaa;
    }
`
const fieldsetCSS = css`
                    margin: 10px auto 0 auto;
                    padding: 30px;
                    width: 350px;
                    background-color: gray;
                    border-radius: 4px;
                    border: 1px solid gray;
                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);
                `
const buttonCss = css`
                            margin: 30px 0px 0px 0px;
                            padding: 20px 0px 0px 0px;   
                            border-top: 1px solid gray;
                        `

let initState = {sitename:''}

export const AddSiteForm = () => {
    const { register, handleSubmit, errors } = useForm<AddSite>();
    const [ siteName, setSitename] = useState<AddSite>(initState);

    const onSubmit = (data: AddSite) => {
        updateData(data.sitename);
    };

    const updateData = (sitename: string): void => {
        //TODO вынести в apiRequests
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/api/rules/addsite');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            alert(data ? 'Site ' + sitename + ' has been added!' : 'Site ' + sitename+' has allready exsists!');
        };
        xhr.send(JSON.stringify(sitename));
    }

    if (!localStorage.jwtToken) return (<Redirect to="/" />)
    return(
        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset css={fieldsetCSS}>
                <div className="field">
                    <label htmlFor="sitename">Site Name (Base Url)</label>
                    <input
                        css={baseCSS}
                        type="text"
                        id="sitename"
                        name="sitename"
                        value={siteName.sitename}
                        onChange={(
                            ev: React.ChangeEvent<HTMLInputElement>,
                        ): void => setSitename({ sitename: ev.target.value })}
                        ref={register({ required: true })}
                    />
                    {errors.sitename && errors.sitename.type === "required" && (<div className="error">Your must enter sitename.</div>)}
                </div>
                <div css={buttonCss}>
                    <button type="submit" className="btn btn-primary btn-lg">Add Site</button>
                </div>
            </fieldset>
        </form>
    );
};