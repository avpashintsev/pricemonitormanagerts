"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddSiteForm = void 0;
var react_hook_form_1 = require("react-hook-form");
var core_1 = require("@emotion/core");
var react_router_1 = require("react-router");
var react_1 = require("react");
var baseCSS = core_1.css(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"], ["\n    box-sizing:border-box;\n    margin-bottom:5px;\n    padding:8px 10px;\n    border: 1px solid gray;\n    border-radius:3px;\n    color:gray;\n    background-color:white;\n    width:100%;\n    :focus{\n        outline-color: #898888;\n    }\n    :disabled{\n        outline-color: #adaaaa;\n    }\n"])));
var fieldsetCSS = core_1.css(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n                    margin: 10px auto 0 auto;\n                    padding: 30px;\n                    width: 350px;\n                    background-color: gray;\n                    border-radius: 4px;\n                    border: 1px solid gray;\n                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);\n                "], ["\n                    margin: 10px auto 0 auto;\n                    padding: 30px;\n                    width: 350px;\n                    background-color: gray;\n                    border-radius: 4px;\n                    border: 1px solid gray;\n                    box-shadow: 0 3px 5px 0 rgba(0,0,0,0.16);\n                "])));
var buttonCss = core_1.css(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n                            margin: 30px 0px 0px 0px;\n                            padding: 20px 0px 0px 0px;   \n                            border-top: 1px solid gray;\n                        "], ["\n                            margin: 30px 0px 0px 0px;\n                            padding: 20px 0px 0px 0px;   \n                            border-top: 1px solid gray;\n                        "])));
var initState = { sitename: '' };
exports.AddSiteForm = function () {
    var _a = react_hook_form_1.useForm(), register = _a.register, handleSubmit = _a.handleSubmit, errors = _a.errors;
    var _b = react_1.useState(initState), siteName = _b[0], setSitename = _b[1];
    var onSubmit = function (data) {
        updateData(data.sitename);
    };
    var updateData = function (sitename) {
        //TODO вынести в apiRequests
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/api/rules/addsite');
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.jwtToken);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            alert(data ? 'Site ' + sitename + ' has been added!' : 'Site ' + sitename + ' has allready exsists!');
        };
        xhr.send(JSON.stringify(sitename));
    };
    if (!localStorage.jwtToken)
        return (core_1.jsx(react_router_1.Redirect, { to: "/" }));
    return (core_1.jsx("form", { onSubmit: handleSubmit(onSubmit) },
        core_1.jsx("fieldset", { css: fieldsetCSS },
            core_1.jsx("div", { className: "field" },
                core_1.jsx("label", { htmlFor: "sitename" }, "Site Name (Base Url)"),
                core_1.jsx("input", { css: baseCSS, type: "text", id: "sitename", name: "sitename", value: siteName.sitename, onChange: function (ev) { return setSitename({ sitename: ev.target.value }); }, ref: register({ required: true }) }),
                errors.sitename && errors.sitename.type === "required" && (core_1.jsx("div", { className: "error" }, "Your must enter sitename."))),
            core_1.jsx("div", { css: buttonCss },
                core_1.jsx("button", { type: "submit", className: "btn btn-primary btn-lg" }, "Add Site")))));
};
var templateObject_1, templateObject_2, templateObject_3;
//# sourceMappingURL=AddSite.js.map