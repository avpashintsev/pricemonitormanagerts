﻿/** @jsx jsx */
import * as React from 'react';
import { css, jsx } from '@emotion/core'

type RuleRowProps = {
    name: string,
    value: string | undefined
    handle: (name: string, value: string) => void;
}
const baseCSS = css`
    box-sizing:border-box;
    margin-bottom:5px;
    padding:8px 10px;
    border: 1px solid gray;
    border-radius:3px;
    color:gray;
    background-color:white;
    width:100%;
    :focus{
        outline-color: #898888;
    }
    :disabled{
        outline-color: #adaaaa;
    }
`

export const RuleTableRow: React.FC<RuleRowProps> = ({ name, value, handle }) => <tr>
    <td><label htmlFor="price">{name} rule:</label></td>
    <td><input
        css={baseCSS}
        type="text"
        id={name}
        name={name}
        value={value}
        onChange={(
            ev: React.ChangeEvent<HTMLInputElement>,
        ): void => handle(name, ev.target.value)}
    /></td>
</tr>