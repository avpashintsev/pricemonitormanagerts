import {
	SET_CURRENT_USER,
	SET_NOT_CONFIRMED_EMAIL,
	CONFIRM_EMAIL,
	GET_USER_DATA,
	RESET
} from './actionTypes';
import isEmpty from 'lodash/isEmpty';

const initialState = {
	// notConfirmedEmail: '',
	isAuthenticated: false,
	user: {},
	info: {},
	wallets: {},
	address: [],
};

export const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_CURRENT_USER:
			return {
				...state,
				isAuthenticated: !isEmpty(action.payload),
				user: action.payload
			};
		case SET_NOT_CONFIRMED_EMAIL:
			return {
				...state,
				notConfirmedEmail: action.payload
			};
		case RESET:
			return initialState;
		default:
			return state;
	}
};