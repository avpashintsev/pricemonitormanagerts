import jwtDecode from 'jwt-decode';
import {
	SET_CURRENT_USER, CONFIRM_EMAIL,
	GET_USER_DATA, USER_DEPOSIT_WALLETS, SET_PASSWORD_RESET_TOKEN
} from './actionTypes';


const {apiRequest} = require('../../utils/apiRequest');
let axiosClient = require('../../utils/axiosClient');

export function setCurrentUser(user) {
	return {type: SET_CURRENT_USER, payload: user};
}

export function confirmEmailSuccess() {
	return {type: CONFIRM_EMAIL};
}

export function setPasswordResetToken(token) {
	return {type: SET_PASSWORD_RESET_TOKEN, payload: token}
}

export function getUserDataRequestSuccess(data) {
	return {type: GET_USER_DATA, payload: data}
}

export function getUserDepositWalletsSuccess(wallets) {
	return {type: USER_DEPOSIT_WALLETS, payload: wallets}
}

export function checkAuth(store) {
	if (localStorage.jwtToken) {
		const tokenExpire = jwtDecode(localStorage.jwtToken).exp;
		const currentTimestamp = Math.floor(Date.now() / 1000);

		if (currentTimestamp > tokenExpire) {
			localStorage.removeItem('jwtToken');
		} else {
			setAuthorizationToken(localStorage.jwtToken);
			store.dispatch(setCurrentUser(jwtDecode(localStorage.jwtToken)));
			//store.dispatch(getUserData());
		}
	}
}

export function setAuthorizationToken(token) {
	if (token) {
		localStorage.setItem('jwtToken', token);
		axiosClient.defaults.headers.common['Auth'] = token;
	} else {
		localStorage.removeItem('jwtToken');
		delete axiosClient.defaults.headers.common['Auth'];
	}
}

export function getUserData() {
	return dispatch => {
		return axiosClient.get('/user/info').then(res => {
				if (res.data.status === 'ok') {
					dispatch(getUserDataRequestSuccess(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
			/*dispatch(showErrorPopup(err.message));*/
				alert('getUserData error ' + err.message);
			}
		);
	}
}

export function signUp(userData) {
	return dispatch => {
		return axiosClient.post('/user/create', userData).then(res => {
				dispatch(hidePopup('signUp'));
				if (res.data.status === 'ok') {
					dispatch(showPopup('confirmEmail'));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(hidePopup('signUp'));
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function logIn(userData) {
	return dispatch => {
		return axiosClient.post('/api/authentication', userData).then(res => {
				if (res.data.status === 'ok') {
					const token = res.data.token;

					setAuthorizationToken(token);

					dispatch(setCurrentUser(jwtDecode(token)));

					//alert('logged');
					window.location = '/addsite'
				} else {
					//dispatch(showErrorPopup(res.data.message));
					alert('error not ok' +res.data.message);
				}
			},
			err => {
				alert(' hidePopup login');
				alert(' error ' + err.message);
			}
		);
	}
}

export function logOut() {
	console.log('logout');

	return dispatch => {
		setAuthorizationToken(false);
		window.location="/"
		//dispatch({type: RESET});
	}
}

export function updateUserData(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		return axiosClient.post('/user/profile-update', data).then(res => {
				if (res.data.status === 'ok') {
					dispatch(showSuccessPopup(t.successfullyUpdated));
					dispatch(getUserData());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function updatePasswords(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		return axiosClient.post('/user/update-password', data).then(res => {
				if (res.data.status === 'ok') {
					dispatch(showSuccessPopup(t.successfullyUpdated));
					dispatch(getUserData());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}


export function confirmEmail(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		axiosClient.post('/user/confirm', data).then(
			res => {
				if (res.data.status === 'ok') {
					dispatch(hidePopup('confirmEmail'));
					dispatch(showSuccessPopup(t.congratulations));
					dispatch(showPopup('logIn'));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			err => {
				dispatch(showErrorPopup(err.message));
			},
		)
	}
}

export function getAddress() {
	return dispatch => {
		return axiosClient.get('/user/address-list').then(res => {
				if (res.data.status === 'ok') {
					dispatch(getAddressSuccess(res.data.data));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function addAddress(data) {
	return (dispatch, getState) => {
		const t = getState().i18n.translations[getState().i18n.locale];

		return axiosClient.post('/user/add-address', data).then(res => {
				if (res.data.status === 'ok') {
					dispatch(getAddress());
					dispatch(hidePopup('addAddress'));
					dispatch(showSuccessPopup( t.congratulationsBTC));
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}

export function removeAddress(id) {
	return dispatch => {
		return axiosClient.post('/user/delete-address', {id}).then(res => {
				if (res.data.status === 'ok') {
					dispatch(getAddress());
				} else {
					dispatch(showErrorPopup(res.data.message));
				}
			},
			(err) => {
				dispatch(showErrorPopup(err.message));
			}
		);
	}
}