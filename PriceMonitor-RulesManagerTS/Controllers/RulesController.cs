﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PriceMonitor_RulesManagerTS.Data;
using PriceMonitor_RulesManagerTS.Models;

namespace PriceMonitor_RulesManagerTS.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RulesController : ControllerBase
    {
        private readonly IDataRepository _dataRepository;

        public RulesController(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpGet("findsite")]
        public IEnumerable<Rules> FindSite(string site)
        {
            return _dataRepository.GetRules(site);
        }
        [HttpPut("updatesite")]
        public IEnumerable<Rules> UpdateSite(Rules rules)
        {
            return _dataRepository.UpdateRules(rules);
        }
        [HttpPost("addsite")]
        public bool AddSite([FromBody] string sitename)
        {
            return _dataRepository.AddSite(sitename);
        }
        [HttpDelete("deletesite")]
        public void DeleteSite([FromBody] string sitename)
        {
            _dataRepository.DeleteSite(sitename);
        }
    }
}
