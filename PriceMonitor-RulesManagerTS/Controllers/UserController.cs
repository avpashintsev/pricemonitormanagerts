﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PriceMonitor_RulesManagerTS.Data;
using PriceMonitor_RulesManagerTS.Models;

namespace PriceMonitor_RulesManagerTS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IDataRepository _dataRepository;

        public UserController(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }
        [HttpGet("getusers")]
        public IEnumerable<User> GetUsers()
        {
            return _dataRepository.GetUsers();
        }

        [HttpGet]
        public bool IsSiteInDb(string url)
        {
            return _dataRepository.IsSiteInDb(url);
        }

        [HttpPost("/login/")]
        public bool Login(string name, string password)
        {
            return _dataRepository.Login(name, password);
        }
    }
}
