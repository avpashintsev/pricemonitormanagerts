﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PriceMonitor_RulesManagerTS.Controllers
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.IdentityModel.Tokens;
    using PriceMonitor_RulesManagerTS.Data;
    using PriceMonitor_RulesManagerTS.Models;
    using PriceMonitor_RulesManagerTS.Services.Authentication;

    public class LoginResponse
    {
        public string token { get; set; }
        public string status { get; set; }
    }



    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IDataRepository _dataRepository;
        public AuthenticationController(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [AllowAnonymous]
        public ActionResult<LoginResponse> Post(
            AuthenticationRequest authRequest,
            [FromServices] IJwtSigningEncodingKey signingEncodingKey)
        {

            if (!_dataRepository.Login(authRequest.Name, authRequest.Password))
            {
                return Unauthorized();
            }

            var claims = new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, authRequest.Name)
            };

            var token = new JwtSecurityToken(
                issuer: "DemoApp",
                audience: "DemoAppClient",
                claims: claims,
                expires: DateTime.Now.AddMinutes(5),
                signingCredentials: new SigningCredentials(
                    signingEncodingKey.GetKey(),
                    signingEncodingKey.SigningAlgorithm)
            );

            string jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
            return new LoginResponse() { token = jwtToken, status = "ok" };
        }
    }
}
