﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PriceMonitor_RulesManagerTS.Models
{
    public class Rules
    {
        public string SiteUrl { get; set; }
        public string PriceRule { get; set; }
        public string Rule1 { get; set; }
        public string Rule2 { get; set; }
    }
}
