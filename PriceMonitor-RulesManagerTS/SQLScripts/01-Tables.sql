﻿USE [price_monitor]
GO

/****** Object:  Table [dbo].[Rules]    Script Date: 24.09.2020 22:11:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Rules](
	[SiteUrl] [nvarchar](30) NOT NULL,
	[PriceRule] [nvarchar](500) NULL,
	[Rule1] [nvarchar](500) NULL,
	[Rule2] [nvarchar](500) NULL
) ON [PRIMARY]
GO

USE [price_monitor]
GO

/****** Object:  Table [dbo].[users]    Script Date: 24.09.2020 22:12:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[users](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nchar](10) NULL
) ON [PRIMARY]
GO


