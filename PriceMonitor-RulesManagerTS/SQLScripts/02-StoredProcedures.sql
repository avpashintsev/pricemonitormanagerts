﻿USE [price_monitor]
GO
/****** Object:  StoredProcedure [dbo].[AddSite]    Script Date: 24.09.2020 22:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddSite] 
@Url varchar(50)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @count INT 
	SET @count = (SELECT count(*) FROM Rules WHERE SiteUrl=@Url)
	IF @count<1 
	INSERT INTO Rules VALUES(@Url,'','','')
    SELECT ((CASE WHEN @count > 0 THEN CAST (0 AS BIT)  ELSE CAST (1 AS BIT)  END))
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteSite]    Script Date: 24.09.2020 22:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteSite] 
@Url varchar(50)
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM Rules WHERE SiteUrl=@Url
END
GO
/****** Object:  StoredProcedure [dbo].[FindSite]    Script Date: 24.09.2020 22:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FindSite] 
@Url varchar(50)
AS
BEGIN
	SET NOCOUNT ON
SELECT [SiteUrl]
      ,[PriceRule]
      ,[Rule1]
      ,[Rule2]
  FROM [price_monitor].[dbo].[Rules] where [SiteUrl] =@Url
END
GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 24.09.2020 22:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUsers]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, Name, Password, Email FROM users
END
GO
/****** Object:  StoredProcedure [dbo].[IsSiteInDb]    Script Date: 24.09.2020 22:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[IsSiteInDb] 
@Url varchar(50)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @count int
    SET @count = (SELECT count(*) FROM Rules WHERE SiteUrl=@Url)
    SELECT ((CASE WHEN @count > 0 THEN CAST (1 AS BIT)  ELSE CAST (0 AS BIT)  END))
END
GO
/****** Object:  StoredProcedure [dbo].[IsUserExsists]    Script Date: 24.09.2020 22:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[IsUserExsists] 
@login varchar(50),
@password varchar(50)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @count int
    SET @count = (SELECT count(*) FROM users WHERE (users.Name=@login and users.Password=@password))
    SELECT ((CASE WHEN @count > 0 THEN CAST (1 AS BIT)  ELSE CAST (0 AS BIT)  END))
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateSite]    Script Date: 24.09.2020 22:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateSite] 
@Url varchar(50),
@PriceRule varchar(50),
@Rule1 varchar(50),
@Rule2 varchar(50)
AS
BEGIN
	SET NOCOUNT ON
	UPDATE Rules
	SET [PriceRule]=@PriceRule
      , [Rule1] = @Rule1
      , [Rule2] = @Rule2
	FROM Rules WHERE SiteUrl=@Url
    SELECT [SiteUrl]
      ,[PriceRule]
      ,[Rule1]
      ,[Rule2] FROM Rules WHERE SiteUrl=@Url
END
GO
