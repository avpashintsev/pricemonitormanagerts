﻿using PriceMonitor_RulesManagerTS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PriceMonitor_RulesManagerTS.Data
{
    public interface IDataRepository
    {
        bool IsUserLoginCorrect(string name, string password);

        void AddSiteToDb(string url);
        bool IsSiteInDb(string url);
        void UpdateSite(Rules rules);
        void DeleteSite(string url);
        bool AddSite(string sitename);
        IEnumerable<User> GetUsers();
        bool Login(string name, string password);
        IEnumerable<Rules> GetRules(string site);
        IEnumerable<Rules> UpdateRules(Rules rules);
    }
}
