﻿using Microsoft.Extensions.Configuration;
using PriceMonitor_RulesManagerTS.Models;
using System;
using Microsoft.Data.SqlClient;
using Dapper;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace PriceMonitor_RulesManagerTS.Data
{
    public class DataRepository : IDataRepository
    {
        private readonly string _connectionString;

        public DataRepository(IConfiguration configuration)
        {
            _connectionString = configuration["ConnectionStrings:DefaultConnection"];
        }

        public void AddSiteToDb(string url)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Rules> GetRules(string site)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return connection.Query<Rules>
                (@"EXEC [dbo].[FindSite] @Url=@Url", new { Url = site });
            }
        }

        public IEnumerable<User> GetUsers()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return connection.Query<User>
                (@"EXEC GetUsers");
            }
        }

        public bool IsSiteInDb(string url)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return connection.QueryFirst<bool>
                (@"EXEC [dbo].[IsSiteInDb] @Url=@Url",new {Url=url});
            }
        }

        public bool IsUserLoginCorrect(string name, string password)
        {
            throw new NotImplementedException();
        }

        public bool Login(string name, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return connection.QueryFirst<bool>
                (@"EXEC [dbo].[IsUserExsists] @login=@Login, @password=@Password", new { Login = name, Password = password });
            }
        }

        public IEnumerable<Rules> UpdateRules(Rules rules)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return connection.Query<Rules>
                (@"EXEC [dbo].[UpdateSite] @Url=@Url, @PriceRule=@price, @Rule1=@r1, @Rule2=@r2", new { 
                    Url = rules.SiteUrl,
                    price = rules.PriceRule,
                    r1 = rules.Rule1,
                    r2 = rules.Rule2
                });                
            }
        }

        public bool AddSite(string sitename)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return connection.QueryFirst<bool>
                (@"EXEC [dbo].[AddSite] @Url=@Url", new
                {
                    Url = sitename
                });
            }
        }

        public void DeleteSite(string sitename)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                connection.Execute
                (@"EXEC [dbo].[DeleteSite] @Url=@Url", new
                {
                    Url = sitename
                });
            }
        }

        public void UpdateSite(Rules rules)
        {
            throw new NotImplementedException();
        }
    }
}
